from enum import Enum


class Side(str, Enum):
    CLIENT = 'client'
    SERVER = 'server'
    SHARED = 'shared'


class TypeDoc(str, Enum):
    FUNCTION = 'func'
    EVENT = 'event'
    CLASS = 'class'
