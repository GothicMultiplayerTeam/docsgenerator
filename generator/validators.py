from typing import List

from .tags import AbstractTag
from .registry import TagRegistry


def tag_required_validate(registry: TagRegistry, tags: List[AbstractTag]):
    identifiers = set(tag.identifier for tag in tags)
    for tag in registry.values():
        if tag.required and tag.identifier not in identifiers:
            raise ValueError(f'Required tag: {tag.identifier}')


def tag_duplicate_validate(tags: List[AbstractTag]):
    entries = set()
    for tag in tags:
        if not tag.duplicate_allowed and tag.identifier in entries:
            raise ValueError(f'Duplicate tag: {tag.identifier}')

        entries.add(tag.identifier)

