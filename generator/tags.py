import re

from abc import ABC
from typing import Any

from .enums import Side
from .models import Param, Return


class AbstractTag(ABC):
    identifier = None
    required = False
    duplicate_allowed = False

    value: Any

    def __init__(self, value: Any):
        self.value = value

    @classmethod
    def parse(cls, text: str):
        ...


class GenericTag(AbstractTag):
    value: str

    @classmethod
    def parse(cls, text: str):
        return cls(text)

    def __str__(self) -> str:
        return self.value


class CategoryTag(GenericTag):
    identifier = 'category'
    required = False
    duplicate_allowed = False
    value: str = None


class ExtendsTag(GenericTag):
    identifier = 'extends'
    required = False
    duplicate_allowed = False
    value: str = None


class NoteTag(GenericTag):
    identifier = 'note'
    required = False
    duplicate_allowed = True


class NameTag(GenericTag):
    identifier = 'name'
    required = True
    duplicate_allowed = False


class VersionTag(GenericTag):
    identifier = 'version'
    required = False
    duplicate_allowed = False
    value: str = None


class DeprecatedTag(GenericTag):
    identifier = 'deprecated'
    required = False
    duplicate_allowed = False
    value: str = None


class CancellableTag(AbstractTag):
    identifier = 'cancellable'
    required = False
    duplicate_allowed = False

    value: bool = False

    @classmethod
    def parse(cls, text: str):
        return cls(True)


class ReadOnlyTag(AbstractTag):
    identifier = 'readonly'
    required = False
    duplicate_allowed = False

    value: bool = False

    @classmethod
    def parse(cls, text: str):
        return cls(True)


class StaticTag(AbstractTag):
    identifier = 'static'
    required = False
    duplicate_allowed = False

    value: bool = False

    @classmethod
    def parse(cls, text: str):
        return cls(True)


class SideTag(AbstractTag):
    identifier = 'side'
    required = True
    duplicate_allowed = False

    value: Side = None

    @classmethod
    def parse(cls, text: str):
        return cls(Side(text))


class ParamTag(AbstractTag):
    identifier = 'param'
    required = False
    duplicate_allowed = True

    value: Param

    @classmethod
    def parse(cls, text: str):
        pattern = re.compile(r'\((.+?)\)\s*?([a-zA-Z0-9_]+|\.\.\.)\s*(=\s*(-?[0-9.e]+|[a-zA-Z0-9_]+|\".+?\"))?\s*(.*)')
        match = pattern.search(text)
        if match is None:
            raise ValueError(f'Invalid pattern of {cls.identifier}: {text}')

        param_type, name, _, default, description = match.groups()
        return cls(Param(param_type, name, default, description.strip()))


class ReturnTag(AbstractTag):
    identifier = 'return'
    required = False
    duplicate_allowed = False

    value: Return = None

    @classmethod
    def parse(cls, text: str):
        pattern = re.compile(r'\((.+?)\)\s*?(.*)')
        match = pattern.search(text)
        if match is None:
            raise ValueError(f'Invalid pattern of {cls.identifier}: {text}')

        return_type, description = match.groups()
        return cls(Return(return_type, description.strip()))
