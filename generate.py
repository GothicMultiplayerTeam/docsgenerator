import argparse
import contextlib
import os
import shutil
import json

from generator.helpers import collect_source_files, find_squirrel_docs
from generator.validators import tag_required_validate, tag_duplicate_validate
from generator.parser import SquirrelDoc
from generator.docs import DOCS, FunctionDocs, EventDocs, GlobalDocs

from scheme.functions import FunctionScheme
from scheme.events import EventScheme
from scheme.classes import ClassScheme, collect_classes
from scheme.consts import ConstScheme, collect_constants
from scheme.globals import GlobalScheme

ROOT_DIR = os.path.dirname(os.path.realpath(__file__))

# json, markdown
parser = argparse.ArgumentParser(description='Docs generator from source code')
parser.add_argument('version', type=str, help='Docs version')
parser.add_argument('sourcedir', type=str, nargs='+', help='Path to source directory')
parser.add_argument('--excludedir', type=str, nargs='*', help='Path to directory which will be exluded from parsing')
parser.add_argument('--builddir', type=str, nargs='?', default="build/", help='Path to build directory')
parser.add_argument('--markdown', action='store_true', help='Build markdown files.')
parser.add_argument('--json', action='store_true', help='Build api.json file.')

if __name__ == '__main__':
    args = parser.parse_args()
    if not args.markdown and not args.json:
        parser.print_help()
        quit(0)

    if args.builddir[-1] != '/':
        args.builddir += "/"

    # Clear directories
    with contextlib.suppress(FileNotFoundError):
        shutil.rmtree(args.builddir)

    print(f"== Parsing paths: ==")
    for path in args.sourcedir:
        print(f"+ {path}")

    if args.excludedir:
        print(f"== Excluded paths: ==")
        for path in args.excludedir:
            print(f"+ {path}")

    files = collect_source_files(args.sourcedir, exclude_paths=args.excludedir)
    print(f"Files found: {len(files)}")
    print(args.version)
    output = {
        "version": args.version,
        "functions": [],
        "events": [],
        "classes": [],
        "constants": [],
        "globals": []
    }

    for file_path in files:
        file_data = None

        with open(file_path, "r") as f:
            with contextlib.suppress(UnicodeDecodeError):
                file_data = f.read()

        if file_data is None:
            continue

        doc_content = find_squirrel_docs(file_data)
        if not doc_content:
            continue

        docs = []
        for doc_type, content in doc_content:
            try:
                doc_definition = DOCS.get(doc_type)
                if doc_definition is None:
                    raise ValueError(f'Invalid doc type: {doc_type}')

                squirrel_doc = SquirrelDoc.parse(doc_definition.registry, content)
                # Validate
                tag_required_validate(doc_definition.registry, squirrel_doc.tags)
                tag_duplicate_validate(squirrel_doc.tags)

                docs.append(doc_definition.build(squirrel_doc.description, squirrel_doc.tags))

            except ValueError as exc:
                print(f'Error while parsing content:\n{content}')
                raise

        functions = [doc for doc in docs if isinstance(doc, FunctionDocs)]
        events = [doc for doc in docs if isinstance(doc, EventDocs)]
        classes = collect_classes(docs)
        constants = collect_constants(docs)
        globals = [doc for doc in docs if isinstance(doc, GlobalDocs)]

        files = []

        scheme = FunctionScheme(ROOT_DIR)
        for doc in functions:
            context = doc.serialize()
            if args.json:
                output["functions"].append(context)

            if args.markdown:
                content = scheme.render(context)
                file_path = args.builddir + scheme.get_path(doc)
                files.append((file_path, content))

        scheme = EventScheme(ROOT_DIR)
        for doc in events:
            context = doc.serialize()
            if args.json:
                output["events"].append(context)

            if args.markdown:
                content = scheme.render(context)
                file_path = args.builddir + scheme.get_path(doc)
                files.append((file_path, content))

        scheme = ClassScheme(ROOT_DIR)
        for doc in classes:
            context = doc.serialize()
            if args.json:
                output["classes"].append(context)

            if args.markdown:
                content = scheme.render(context)
                file_path = args.builddir + scheme.get_path(doc)
                files.append((file_path, content))

        scheme = GlobalScheme(ROOT_DIR)
        for doc in globals:
            context = doc.serialize()
            if args.json:
                output["globals"].append(context)

            if args.markdown:
                content = scheme.render(context)
                file_path = args.builddir + scheme.get_path(doc)
                files.append((file_path, content))

        scheme = ConstScheme(ROOT_DIR)
        for doc in constants:
            context = doc.serialize()
            if args.json:
                output["constants"].append(context)

            if args.markdown:
                content = scheme.render(context)
                file_path = args.builddir + scheme.get_path(doc)
                files.append((file_path, content))

        if args.markdown:
            for path, content in files:
                with contextlib.suppress(FileExistsError):
                    os.makedirs(os.path.dirname(path))

                with open(path, "w") as f:
                    f.write(content)

                print(f"Doc generated: {path}")

    if args.json:
        print("Generating api.json")
        with open('api.json', 'w') as file:
            json.dump(output, file, indent=4, sort_keys=True)

