from generator.helpers import path_from_text
from generator.scheme import BaseScheme
from generator.docs import EventDocs


class EventScheme(BaseScheme):
    template = 'templates/event.md'

    @staticmethod
    def get_path(docs: EventDocs) -> str:
        root_path = f'{docs.side.value}-events'
        sub_path = path_from_text(docs.category) if docs.category else "general"
        
        return f'{root_path}/{sub_path}/{docs.name}.md'




