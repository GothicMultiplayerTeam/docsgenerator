from typing import List
from itertools import groupby

from generator.helpers import path_from_text
from generator.scheme import BaseScheme
from generator.docs import Docs, ConstDocs


class ConstGroup(list):
    def serialize(self):
        return {
            'category': self[0].category,
            'side': self[0].side,
            'elements': [const.serialize() for const in self]
        }


def collect_constants(docs: List[Docs]):
    # Filter irrelevant types
    docs = [doc for doc in docs if isinstance(doc, ConstDocs)]
    docs = sorted(docs, key=lambda x: (x.category, x.side.value))

    constants = []
    for category, items in groupby(docs, key=lambda x: (x.category, x.side.value)):
        constants.append(ConstGroup(items))

    return constants


class ConstScheme(BaseScheme):
    template = 'templates/const.md'

    @staticmethod
    def get_path(docs: ConstGroup) -> str:
        entry = docs[0]

        root_path = f'{entry.side.value}-constants'
        sub_path = path_from_text(entry.category) if entry.category else "general"

        return f'{root_path}/{sub_path}.md'
