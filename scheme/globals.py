from generator.helpers import path_from_text
from generator.scheme import BaseScheme
from generator.docs import GlobalDocs


class GlobalScheme(BaseScheme):
    template = 'templates/global.md'

    @staticmethod
    def get_path(docs: GlobalDocs) -> str:
        root_path = f'{docs.side.value}-globals'
        return f'{root_path}/{path_from_text(docs.name)}.md'
