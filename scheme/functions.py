from generator.helpers import path_from_text
from generator.scheme import BaseScheme
from generator.docs import FunctionDocs


class FunctionScheme(BaseScheme):
    template = 'templates/function.md'

    @staticmethod
    def get_path(docs: FunctionDocs) -> str:
        root_path = f'{docs.side.value}-functions'
        sub_path = path_from_text(docs.category) if docs.category else "general"
        
        return f'{root_path}/{sub_path}/{docs.name}.md'
